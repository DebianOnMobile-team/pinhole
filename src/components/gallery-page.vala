/* gallery-page.vala
 *
 * Copyright 2020 James Westman <james@flyingpimonster.net>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */


/**
 * A page in a #CameraGallery.
 *
 * Images are simply displayed. Videos show a thumbnail, which can be clicked
 * to play the video.
 */
public class Camera.GalleryPage : Gtk.EventBox {
    public File file { get; construct; }
    public Gdk.Pixbuf pixbuf { get; construct; }


    private Gtk.DrawingArea da;

    private CachedSurface cached_surface;


    /**
     * Creates a new image #CameraGalleryPage with the given image.
     */
    public GalleryPage.for_image(Gdk.Pixbuf pixbuf) {
        Object(pixbuf: pixbuf);
    }

    construct {
        this.da = new Gtk.DrawingArea();
        this.da.visible = true;
        this.da.draw.connect(this.on_draw);
        this.add(this.da);

        if (this.pixbuf != null) {
            cached_surface = new CachedSurface.with_pixbuf(pixbuf);
        }
    }


    private bool on_draw(Cairo.Context ctx) {
        if (this.cached_surface != null) {
            double width = this.get_allocated_width();
            double height = this.get_allocated_height();

            double w = this.pixbuf.get_width();
            double h = this.pixbuf.get_height();
            scale_to_fit(ref w, ref h, width, height);

            double x, y;
            center(w, h, width, height, out x, out y);

            cached_surface.draw(ctx, get_scale_factor(), x, y, (int) w, (int) h);
        }

        return false;
    }
}

