/* controls-group.vala
 *
 * Copyright 2020 James Westman <james@flyingpimonster.net>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */


public class Camera.ControlsGroup : Object {
    public double gallery_progress { get; set; }
    public int shutter_countdown { get; set; }
    public bool switcher_sensitive { get; set; }
    public ShutterButtonMode shutter_mode { get; set; }


    private List<Controls> controls_list;


    public void setup(Gallery gallery) {
        foreach (var controls in controls_list) {
            controls.setup(gallery);
        }
    }


    public void add_controls(Controls controls) {
        controls_list.append(controls);

        bind_property("gallery_progress", controls, "gallery_progress", DEFAULT | SYNC_CREATE);
        bind_property("shutter_countdown", controls, "shutter_countdown", DEFAULT | SYNC_CREATE);
        bind_property("switcher_sensitive", controls, "switcher_sensitive", DEFAULT | SYNC_CREATE);
        bind_property("shutter_mode", controls, "shutter_mode", DEFAULT | SYNC_CREATE);
    }


    public void start_countdown() {
        foreach (var controls in controls_list) {
            controls.start_countdown();
        }
    }


    public void stop_countdown() {
        foreach (var controls in controls_list) {
            controls.stop_countdown();
        }
    }


    public void popdown_countdown_button() {
        foreach (var controls in controls_list) {
            controls.popdown_countdown_button();
        }
    }
}
