/* main.vala
 *
 * Copyright 2020 James Westman
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


int main (string[] args) {
    Environment.set_variable("GST_DEBUG_DUMP_DOT_DIR", Environment.get_user_special_dir(DESKTOP), true);

    Aperture.init(ref args);

    // Ensure types that are needed by GtkBuilder
    typeof(Camera.LayerContainer).ensure();

    var app = new Gtk.Application (Camera.APPLICATION_ID, ApplicationFlags.FLAGS_NONE);

    app.startup.connect(() => {
        Hdy.init();

        /* Make sure icons are in the icon theme's path, even if we're using the
         * .Devel app ID */
        Gtk.IconTheme.get_default().add_resource_path("/net/flyingpimonster/Camera/icons");

        var css = new Gtk.CssProvider();
        css.load_from_resource("/net/flyingpimonster/Camera/ui/styles.css");
        Gtk.StyleContext.add_provider_for_screen(Gdk.Screen.get_default(), css, 600);

        var gtk_settings = Gtk.Settings.get_default();
        gtk_settings.gtk_application_prefer_dark_theme = true;

        var quit = new SimpleAction("quit", null);
        quit.activate.connect(() => app.quit());
        app.add_action(quit);

        app.set_accels_for_action("win.escape", { "Escape" });
        app.set_accels_for_action("win.go-left", { "Left" });
        app.set_accels_for_action("win.go-right", { "Right" });
        app.set_accels_for_action("win.take-picture", { "space" });
        app.set_accels_for_action("win.next-camera", { "<Primary>N" });
        app.set_accels_for_action("win.picture-mode", { "<Primary>P" });
        app.set_accels_for_action("win.video-mode", { "<Primary>V" });
        app.set_accels_for_action("app.quit", { "<Primary>Q" });
    });

    app.activate.connect (() => {
        var win = app.active_window;
        if (win == null) {
            win = new Camera.Window (app);
        }
        win.present ();
    });

    return app.run (args);
}
