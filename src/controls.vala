/* controls.vala
 *
 * Copyright 2020 James Westman <james@flyingpimonster.net>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */


[GtkTemplate (ui = "/net/flyingpimonster/Camera/ui/controls.ui")]
public class Camera.Controls : ControlsContainer {
    private double _gallery_progress;
    public double gallery_progress {
        get {
            return _gallery_progress;
        }
        set {
            _gallery_progress = value;

            center_buttons.opacity = 1 - gallery_progress;
            center_buttons.sensitive = gallery_progress < 1;
            mode_box.opacity = 1 - gallery_progress;
            back_button.opacity = gallery_progress;
            back_button.visible = gallery_progress > 0;
        }
    }

    public int shutter_countdown {
        get {
            return shutter.countdown;
        }
        set {
            shutter.countdown = value;
        }
    }


    public bool switcher_sensitive {
        get {
            return switcher.sensitive;
        }
        set {
            switcher.sensitive = value;
        }
    }


    public ShutterButtonMode shutter_mode {
        get {
            return shutter.mode;
        }
        set {
            shutter.mode = value;
        }
    }

    public bool show_center_buttons { get; set; }

    public bool show_other_buttons { get; set; }


    [GtkChild] private Gtk.Box center_buttons;
    [GtkChild] private Gtk.Box mode_box;
    [GtkChild] private Gtk.Button back_button;
    [GtkChild] private Gtk.MenuButton countdown_button;
    [GtkChild] public GalleryButton gallery_button;
    [GtkChild] public Gtk.Button switcher;
    [GtkChild] public ShutterButton shutter;
    [GtkChild] public Gtk.Button close_button;


    construct {
        var devices = Aperture.DeviceManager.get_instance();
        devices.camera_added.connect(on_camera_list_changed);
        devices.camera_removed.connect(on_camera_list_changed);
        on_camera_list_changed();

        bind_property("orientation", center_buttons, "orientation", DEFAULT | SYNC_CREATE);
        bind_property("orientation", mode_box, "orientation", DEFAULT | SYNC_CREATE);

        Gtk.Settings.get_default().notify["gtk-decoration-layout"].connect(on_decoration_layout_changed);
        on_decoration_layout_changed();
    }


    public Controls(ControlsContainer.Mode mode) {
        Object(mode: mode);
    }


    public void setup(Gallery gallery) {
        gallery_button.gallery = gallery;
    }


    public void activate_camera_switcher() {
        switcher.activate();
    }


    public void start_countdown() {
        shutter.start_countdown();
    }


    public void stop_countdown() {
        shutter.stop_countdown();
    }


    public void popdown_countdown_button() {
        countdown_button.popover.popdown();
    }


    private void on_camera_list_changed() {
        var devices = Aperture.DeviceManager.get_instance();
        switcher.opacity = (devices.num_cameras > 1) ? 1 : 0;
    }


    private void on_decoration_layout_changed() {
        close_button.visible = "close" in Gtk.Settings.get_default().gtk_decoration_layout;
    }
}
